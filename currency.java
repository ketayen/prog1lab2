//Programmer: Kevin Nguyen
//Objectif:transfering the CAD currecy to US and EURO


import java.util.Scanner;

public class Currency
{
	public static void main(String[] args) {
		//prompt user to input
       System.out.print("Enter CAD amount: ");
		Scanner kb = new Scanner(System.in);
        //input value
        double cad = kb.nextDouble();
		double usd = cad * 0.75;
        double eur = cad * 0.69;
        //printing value
		System.out.println(usd + "USD");
		System.out.println(eur + "EUR");

	}
}