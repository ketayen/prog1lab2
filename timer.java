//Programmer: Kevin Nguyen
//Objectif: Calculating how much time (min:sec) of the total seconds input




import java.util.Scanner;

public class Timer
{
	public static void main(String[] args) {

		//asking user to input
		System.out.print("Enter an amount of seconds: ");
		Scanner kb = new Scanner(System.in);
       //declaring value of amount inputted
		int second = kb.nextInt();
       //calculation
	    int min = second / 60;
	    int sec = second % 60;

	    System.out.println(min + ":" + sec );


	}
}