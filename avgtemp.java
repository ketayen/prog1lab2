//Programmer: Kevin Nguyen
//Objectif: Calculating the average tempature of the 3 following days

import java.util.Scanner;

public class AvgTemp
{
	public static void main(String[] args) {

		//first prompted value
		System.out.print("Enter first day tempature in celsius: ");
		Scanner kb = new Scanner(System.in);
        double d1 = kb.nextInt();

        //second prompted value
		System.out.print("Enter second day tempature in celsius: ");
		Scanner kc = new Scanner(System.in);
        double d2 = kc.nextInt();

        //third prompted value
		System.out.print("Enter third day tempature in celsius: ");
		Scanner kd = new Scanner(System.in);
        double d3 = kd.nextInt();

        //calculation
	    double averageTempature = (d1 + d2 + d3) / 3;

	    System.out.println("The average tempature of the last 3 days is: " + averageTempature + "C");

	}
}

