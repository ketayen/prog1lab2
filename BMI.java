//Programmer: Kevin Nguyen
//Objectif:Calculating the BMI of an individual by using the height and weight inputted



import java.util.Scanner;

public class BMI
{
	public static void main(String[] args) {

		//prompt the user to enter pound
		System.out.print("Enter your weights in pound: ");
		Scanner kb = new Scanner(System.in);

		//calculation
		double pound = kb.nextDouble();
        double kilo = pound * 0.453592;

        //prompt user to enter other feet
		System.out.print("Enter your height in feet: ");
		Scanner kc = new Scanner(System.in);

		//calculation
		double feet = kc.nextDouble();
        double meter = feet * 0.3048;



		double bmi = kilo / Math.pow(meter, 2);
        //printing
		System.out.print("Your BMI is ");
		System.out.printf("%.2f%n",  bmi);
	}
}


